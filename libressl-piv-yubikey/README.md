# Utilizing LIBRESSL, PKCS#11/PKCS#15, and a PIV-II-Capable FIPS 140-2-Compliant Yubikey to run various Smart-Card Cryptographic Operations during Criminal Investigations on OpenBSD 

![Screenshot of a LIBRESSL command output](libressl-cmd-output.png) 

The article above can be found on my website: https://mwm.is/article-libressl.html

